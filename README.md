# Mateus Matzkin Gurza Web Profile
### This Repo was used for the project of developing my own web profile page
---
The web profile must contain my relevant social medias, my contact information, some description of myself and my curriculum vitae, both the portuguese versio and english version

##### The framework used to build the true website was HUGO. Other than learning the HUGO framework, HTML and CSS were programming languages/hardskills learned and developed in this project.

Here is a quick description of the file structure of the project:
- _MyHugoSite_ contains the whole file system associated with the framework and represents the true site to be published.
- _documents_ contains my Curriculum Vitae
- _images_ contains the png, jpeg and jpg files associated with the site
- there is nothing inside the _themes_ folder because the theme was fully developed from scratch
- _layouts_ contains an _experience_ and a _schooling_ folder wich have their own single pages
- _content_ contains all text files in markdown
- All other files are supporting files for the previously mentioned ones
